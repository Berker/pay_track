import 'package:flutter/material.dart';

class OverviewGridCard extends StatelessWidget {
  final String title;
  final double amount;
  final dynamic icon;
  final Color? color;

  OverviewGridCard(this.title, this.amount, this.icon, this.color);

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 8.0,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(3),
          ),
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: FittedBox(
                child: Text(
                  title,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Divider(
              color: Colors.grey,
              indent: 30,
              endIndent: 30,
            ),
            ListTile(
              title: Text(
                amount.toStringAsFixed(0),
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
              subtitle: Text(
                "Euro",
                style: TextStyle(fontSize: 14),
              ),
              leading: IconButton(
                icon: Icon(
                  icon,
                  color: color,
                ),
                onPressed: () {},
              ),
            ),
          ],
        ));
  }
}
