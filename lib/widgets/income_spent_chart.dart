import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:pay_track/providers/transactions.dart';
import 'package:provider/provider.dart';

class IncomeSpentChart extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => IncomeSpentChartState();
}

class IncomeSpentChartState extends State {
  int touchedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 250,
      padding: EdgeInsets.all(8),
      child: PieChart(
        PieChartData(
            pieTouchData: PieTouchData(touchCallback: (pieTouchResponse) {
              setState(() {
                final desiredTouch =
                    pieTouchResponse.touchInput is! PointerExitEvent &&
                        pieTouchResponse.touchInput is! PointerUpEvent;
                if (desiredTouch && pieTouchResponse.touchedSection != null) {
                  touchedIndex =
                      pieTouchResponse.touchedSection!.touchedSectionIndex;
                } else {
                  touchedIndex = -1;
                }
              });
            }),
            borderData: FlBorderData(
              show: false,
            ),
            sectionsSpace: 0,
            centerSpaceRadius: 0,
            sections: showingSections()),
      ),
    );
  }

  List<PieChartSectionData> showingSections() {
    final transactions = Provider.of<Transactions>(context);
    //var remaining = 100 - transactions.getTotalPaymentPercentage().ceil();
    //var percentage = transactions.getTotalPaymentPercentage().ceil();

    return List.generate(2, (i) {
      final isTouched = i == touchedIndex;
      final double fontSize = isTouched ? 18 : 14;
      final double radius = isTouched ? 110 : 100;

      switch (i) {
        case 0:
          return PieChartSectionData(
            color: Colors.lightGreen,
            value: 100 - transactions.getTotalPaymentPercentage(),
            title:
                '${(100 - transactions.getTotalPaymentPercentage()).toStringAsFixed(2)}%',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
            badgeWidget: CircleAvatar(
                radius: 18,
                backgroundColor: Colors.black,
                child: CircleAvatar(
                    radius: 15,
                    backgroundColor: Colors.white,
                    child: Icon(
                      Icons.arrow_downward,
                      color: Colors.lightGreen,
                    ))),
            badgePositionPercentageOffset: .98,
          );
        case 1:
          return PieChartSectionData(
            color: Colors.red,
            value: transactions.getTotalPaymentPercentage(),
            title:
                '${transactions.getTotalPaymentPercentage().toStringAsFixed(2)}%',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
            badgeWidget: CircleAvatar(
                radius: 18,
                backgroundColor: Colors.black,
                child: CircleAvatar(
                    radius: 15,
                    backgroundColor: Colors.white,
                    child: Icon(
                      Icons.arrow_upward,
                      color: Colors.red,
                    ))),
            badgePositionPercentageOffset: .98,
          );
        default:
          return PieChartSectionData();
      }
    });
  }
}
