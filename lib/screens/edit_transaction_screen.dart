import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';
import 'package:flutter/rendering.dart';
import 'package:pay_track/providers/transaction.dart';
import 'package:pay_track/providers/transactions.dart';
import 'package:provider/provider.dart';

class EditTransactionScreen extends StatefulWidget {
  static const routeName = "/edit-transaction-screen";

  @override
  _EditTransactionScreenState createState() => _EditTransactionScreenState();
}

class _EditTransactionScreenState extends State<EditTransactionScreen> {
  final _descriptionFocusNode = FocusNode();
  final _amountFocusNode = FocusNode();
  final _form = GlobalKey<FormState>();
  Map<String, dynamic> addedTags = {};

  final _newTagController = TextEditingController();
  TransactionFrequency? _frequency = TransactionFrequency.Yearly;
  TransactionType? _type = TransactionType.Income;
  var _newTransaction = Transaction(
    id: Uuid().v4(),
    title: "",
    desc: "",
    amount: 0,
    tags: [],
    type: TransactionType.Income,
    frequency: TransactionFrequency.Yearly,
  );

  bool _saveTransaction() {
    final isValid = _form.currentState!.validate();
    if (isValid) {
      if (addedTags.isEmpty) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text("Please at least add one tag."),
            action: SnackBarAction(
              label: "Ok",
              onPressed: () {
                return;
              },
            ),
          ),
        );
        return false;
      }
      if (!addedTags.values.contains(true)) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text("Please choose at least one tag."),
            action: SnackBarAction(
              label: "Ok",
              onPressed: () {
                return;
              },
            ),
          ),
        );
        return false;
      }
      _form.currentState!.save();
      return true;
    }
    return false;
  }

  @override
  void dispose() {
    super.dispose();
    _descriptionFocusNode.dispose();
    _amountFocusNode.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final transactions = Provider.of<Transactions>(context);

    void getTagsAndValues() {
      transactions.allTags.forEach((tag) {
        addedTags.putIfAbsent(tag, () => false);
      });
      @override
      void dispose() {
        super.dispose();
      }
    }

    if (addedTags.isEmpty) {
      getTagsAndValues();
    }

    void _submitTag() {
      if (_newTagController.text.isEmpty) {
        Navigator.of(context).pop();
      }
      final _newTag = _newTagController.text;
      addedTags.putIfAbsent(_newTag, () => true);
      _newTagController.clear();
      Navigator.of(context).pop();
      setState(() {});
    }

    Widget newTagDialog() {
      return AlertDialog(
        title: Text("Enter tag name:"),
        content: TextField(
          decoration: InputDecoration(
            labelText: "Tag",
            border: OutlineInputBorder(),
          ),
          controller: _newTagController,
          //onSubmitted: (_) => _submitTag,
        ),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
              _newTagController.clear();
            },
            child: Text("CANCEL"),
          ),
          TextButton(
            onPressed: () {
              _submitTag();
            },
            child: Text("DONE"),
          ),
        ],
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Transaction"),
      ),
      body: Padding(
        padding: EdgeInsets.all(16),
        child: Form(
          key: _form,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 3.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      labelText: "Title",
                      border: OutlineInputBorder(),
                    ),
                    textInputAction: TextInputAction.next,
                    onFieldSubmitted: (_) {
                      FocusScope.of(context)
                          .requestFocus(_descriptionFocusNode);
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please provide a title.";
                      }
                      return null;
                    },
                    onSaved: (value) {
                      _newTransaction = Transaction(
                        id: _newTransaction.id,
                        title: value.toString(),
                        desc: _newTransaction.desc,
                        amount: _newTransaction.amount,
                        frequency: _newTransaction.frequency,
                        type: _newTransaction.type,
                        tags: _newTransaction.tags,
                      );
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      labelText: "Description",
                      border: OutlineInputBorder(),
                    ),
                    maxLines: 3,
                    textInputAction: TextInputAction.next,
                    onFieldSubmitted: (_) {
                      FocusScope.of(context).requestFocus(_amountFocusNode);
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please provide a description";
                      }
                      if (value.length < 10) {
                        return "Description should be longer then 10 characters.";
                      }
                      return null;
                    },
                    onSaved: (value) {
                      _newTransaction = Transaction(
                        id: _newTransaction.id,
                        title: _newTransaction.title,
                        desc: value.toString(),
                        amount: _newTransaction.amount,
                        frequency: _newTransaction.frequency,
                        type: _newTransaction.type,
                        tags: _newTransaction.tags,
                      );
                      ;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      labelText: "Amount",
                      border: OutlineInputBorder(),
                    ),
                    textInputAction: TextInputAction.next,
                    keyboardType: TextInputType.number,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please provide an amount.";
                      }
                      if (double.tryParse(value) == null) {
                        return "Please provide a valid number.";
                      }
                      if (double.tryParse(value)! < 0) {
                        return "Please provide a number greater then zero.";
                      }
                      return null;
                    },
                    onSaved: (value) {
                      _newTransaction = Transaction(
                        id: _newTransaction.id,
                        title: _newTransaction.title,
                        desc: _newTransaction.desc,
                        amount: double.parse(value.toString()),
                        frequency: _newTransaction.frequency,
                        type: _newTransaction.type,
                        tags: _newTransaction.tags,
                      );
                      ;
                    },
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.only(top: 8, left: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Tags",
                        style: TextStyle(color: Colors.grey),
                      ),
                      IconButton(
                        icon: Icon(Icons.add),
                        onPressed: () {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return newTagDialog();
                              });
                        },
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: double.infinity,
                  height: 50,
                  child: addedTags.isEmpty
                      ? Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text("Add some tags with plus button"),
                        )
                      : ListView(
                          padding: EdgeInsets.all(8),
                          scrollDirection: Axis.horizontal,
                          children: addedTags.keys
                              .map((tag) => Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: FilterChip(
                                      selected: addedTags[tag],
                                      label: Text(tag),
                                      onSelected: (bool value) {
                                        setState(() {
                                          addedTags[tag] = value;
                                        });
                                      },
                                    ),
                                  ))
                              .toList(),
                        ),
                ),
                Divider(color: Colors.black),
                Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.only(left: 8),
                  child: Text(
                    "Frequency",
                    style: TextStyle(color: Colors.grey),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 8, left: 8),
                  height: 40,
                  width: double.infinity,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Expanded(
                        child: ListTile(
                          horizontalTitleGap: 1,
                          visualDensity: VisualDensity.compact,
                          title: Text("Yearly"),
                          leading: Radio<TransactionFrequency>(
                            value: TransactionFrequency.Yearly,
                            groupValue: _frequency,
                            onChanged: (TransactionFrequency? value) {
                              setState(() {
                                _frequency = value;
                              });
                            },
                          ),
                        ),
                      ),
                      Expanded(
                        child: ListTile(
                          horizontalTitleGap: 1,
                          visualDensity: VisualDensity.compact,
                          title: Text("Monthly"),
                          leading: Radio<TransactionFrequency>(
                            value: TransactionFrequency.Monthly,
                            groupValue: _frequency,
                            onChanged: (TransactionFrequency? value) {
                              setState(() {
                                _frequency = value;
                              });
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Divider(height: 30, color: Colors.black),
                Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.only(left: 8),
                  child: Text(
                    "Type",
                    style: TextStyle(color: Colors.grey),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 8, left: 8),
                  height: 40,
                  width: double.infinity,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Expanded(
                        child: ListTile(
                          horizontalTitleGap: 1,
                          visualDensity: VisualDensity.compact,
                          title: Text("Income"),
                          leading: Radio<TransactionType>(
                            value: TransactionType.Income,
                            groupValue: _type,
                            onChanged: (TransactionType? value) {
                              setState(() {
                                _type = value;
                              });
                            },
                          ),
                        ),
                      ),
                      Expanded(
                        child: ListTile(
                          horizontalTitleGap: 1,
                          visualDensity: VisualDensity.compact,
                          title: Text("Payment"),
                          leading: Radio<TransactionType>(
                            value: TransactionType.Payment,
                            groupValue: _type,
                            onChanged: (TransactionType? value) {
                              setState(() {
                                _type = value;
                              });
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Divider(
                  color: Colors.black,
                  height: 30,
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.save),
        onPressed: () {
          if (_saveTransaction()) {
            addedTags.forEach((key, value) {
              if (value) {
                _newTransaction.tags.add(key);
              }
            });
            _newTransaction = Transaction(
              id: _newTransaction.id,
              title: _newTransaction.title,
              desc: _newTransaction.desc,
              amount: _newTransaction.amount,
              frequency: _frequency as TransactionFrequency,
              type: _type as TransactionType,
              tags: _newTransaction.tags,
            );

            transactions.addTransaction(_newTransaction);
            addedTags.keys.forEach((tag) {
              if (!transactions.allTags.contains(tag)) {
                transactions.allTags.add(tag);
              }
            });
            Navigator.of(context).pop();
            setState(() {});
          }
        },
      ),
    );
  }
}
