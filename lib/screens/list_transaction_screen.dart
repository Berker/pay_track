import 'package:flutter/material.dart';
import 'package:pay_track/providers/transaction.dart';
import 'package:pay_track/providers/transactions.dart';
import 'package:provider/provider.dart';

class ListTransactionScreen extends StatefulWidget {
  static const routeName = "/list-transaction-screen";

  @override
  _ListTransactionScreenState createState() => _ListTransactionScreenState();
}

// stores ExpansionPanel state information
class Item {
  Item({
    required this.trans,
    required this.headerValue,
    this.isExpanded = false,
  });

  Transaction trans;
  String headerValue;
  bool isExpanded;
}

class _ListTransactionScreenState extends State<ListTransactionScreen> {
  Map<String, dynamic> addedTags = {};
  var allSelected = true;
  var _isInit = false;
  List<Item> panelItems = [];

  @override
  Widget build(BuildContext context) {
    final transactions = Provider.of<Transactions>(context);
    var selectAllButtonText = allSelected ? "unselect all" : "select all";

    void updatePanelItems() {
      List<Item> updatedPanel = [];
      List<String> _selectedTags = [];
      addedTags.forEach((key, value) {
        if (value == true) {
          _selectedTags.add(key);
        }
      });
      transactions
          .transactionsPerTag(_selectedTags)
          .values
          .forEach((transaction) {
        updatedPanel
            .add(Item(trans: transaction, headerValue: transaction.title));
      });
      panelItems = updatedPanel;
    }

    Widget _deleteConfirmDialog(String id, String title) {
      return AlertDialog(
        title: Text("Are you sure ?"),
        content: Text("Do you want to remove $title ?"),
        actions: [
          TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text("No")),
          TextButton(
              onPressed: () {

                transactions.deleteTransaction(id, "user_transactions");
                panelItems.removeWhere((item) => item.trans.id == id);
                transactions.fetchAndSetTransactionsAndTags();
                addedTags = {};
                panelItems = [];
                setState(() {
                  updatePanelItems();
                  Navigator.of(context).pop();
                });

              },
              child: Text("Yes"))
        ],
      );
    }

    transactions.allTags.forEach((tag) {
      addedTags.putIfAbsent(tag, () => true);
    });

    if (!_isInit) {
      transactions
          .transactionsPerTag(addedTags.keys.toList())
          .values
          .forEach((transaction) {
        panelItems
            .add(Item(trans: transaction, headerValue: transaction.title));
      });
      transactions.fetchAndSetTransactionsAndTags();
      _isInit = true;
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Transaction List"),
      ),
      body: addedTags.isEmpty
          ? Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Center(
                child: Text("Nothing to show yet! Add some transactions first!",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 16)),
              ),
            )
          : Column(
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.only(top: 8, left: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Tags",
                        style: TextStyle(color: Colors.grey),
                      ),
                      TextButton(
                          onPressed: () {
                            setState(() {
                              allSelected = !allSelected;
                              addedTags.updateAll((key, value) => allSelected);
                              updatePanelItems();
                            });
                          },
                          child: Text(selectAllButtonText))
                    ],
                  ),
                ),
                SizedBox(
                  width: double.infinity,
                  height: 50,
                  child: ListView(
                    padding: EdgeInsets.all(8),
                    scrollDirection: Axis.horizontal,
                    children: addedTags.keys
                        .map((tag) => Padding(
                              padding: const EdgeInsets.only(right: 8.0),
                              child: FilterChip(
                                selected: addedTags[tag],
                                label: Text(tag),
                                onSelected: (bool value) {
                                  transactions.fetchAndSetTransactionsAndTags();
                                  setState(() {
                                    addedTags[tag] = value;
                                    updatePanelItems();
                                  });
                                },
                              ),
                            ))
                        .toList(),
                  ),
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Container(
                      child: ExpansionPanelList(
                        elevation: 8,
                        animationDuration: Duration(milliseconds: 700),
                        expansionCallback: (int index, bool isExpanded) {
                          setState(() {
                            panelItems[index].isExpanded = !isExpanded;
                          });
                        },
                        children: panelItems.map<ExpansionPanel>((Item item) {
                          return ExpansionPanel(
                            canTapOnHeader: true,
                            headerBuilder:
                                (BuildContext context, bool isExpanded) {
                              return ListTile(
                                leading: Chip(
                                  backgroundColor:
                                      item.trans.type == TransactionType.Income
                                          ? Colors.lightGreen
                                          : Colors.red,
                                  label: Text(item.trans.amount.toString()),
                                  avatar:
                                      item.trans.type == TransactionType.Income
                                          ? CircleAvatar(
                                              backgroundColor: Colors.white,
                                              child: Icon(
                                                Icons.arrow_upward,
                                                color: Colors.lightGreen,
                                              ))
                                          : CircleAvatar(
                                              backgroundColor: Colors.white,
                                              child: Icon(
                                                Icons.arrow_downward,
                                                color: Colors.red,
                                              )),
                                ),
                                title: Text(item.headerValue),
                              );
                            },
                            body: ListTile(
                              title: Text(item.trans.desc),
                              subtitle: Padding(
                                padding: const EdgeInsets.only(
                                    top: 8.0, bottom: 8.0),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                            "Type: ${item.trans.type == TransactionType.Income ? "Income" : "Payment"}"),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                            "Frequency: ${item.trans.frequency == TransactionFrequency.Monthly ? "Monthly" : "Yearly"}"),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Text("Tags: "),
                                        Row(
                                          children: item.trans.tags.map((tag) {
                                            return Text("$tag ");
                                          }).toList(),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              trailing: Container(
                                width: 100,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    IconButton(
                                        icon: Icon(
                                          Icons.edit,
                                          color: Colors.blue,
                                        ),
                                        onPressed: () {}),
                                    IconButton(
                                        icon: Icon(
                                          Icons.delete,
                                          color: Colors.red,
                                        ),
                                        onPressed: () {
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return _deleteConfirmDialog(
                                                    item.trans.id,
                                                    item.trans.title);
                                              });
                                        }),
                                  ],
                                ),
                              ),
                            ),
                            isExpanded: item.isExpanded,
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                ),
              ],
            ),
    );
  }
}
