import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:pay_track/screens/edit_transaction_screen.dart';
import 'package:pay_track/widgets/overview_grid_card.dart';
import 'package:provider/provider.dart';

import '../widgets/income_spent_chart.dart';

import '../providers/transactions.dart';

class OverviewScreen extends StatefulWidget {
  static const routeName = "/overview_screen";

  @override
  _OverviewScreenState createState() => _OverviewScreenState();
}



class _OverviewScreenState extends State<OverviewScreen> {
  bool _isInit = false;

  @override
  Widget build(BuildContext context) {
    final transactions = Provider.of<Transactions>(context);
    if (!_isInit) {
      transactions.fetchAndSetTransactionsAndTags();
      _isInit = true;
    }

    Widget _buildChart() {
      return Card(
          elevation: 8,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(3))),
          color: Colors.white,
          child: transactions.totalYearlyIncome() == 0.0 ||
                  transactions.totalYearlyPayments() == 0.0
              ? Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "To see the charts, please add some yearly income and payments.",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                )
              : IncomeSpentChart());
    }

    return Scaffold(
        appBar: AppBar(
          title: Text("PayTrack - Overview"),
        ),
        body: transactions.items.isEmpty
            ? Center(
                child: !_isInit ? CircularProgressIndicator() : Text(
                  "Nothing to show! Add some transactions first!",
                  textAlign: TextAlign.center, style: TextStyle(fontSize: 16),
                ),
              )
            : SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Card(
                      elevation: 8,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(3)),
                      ),
                      child: IntrinsicHeight(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Column(
                              children: [
                                Icon(Icons.add),
                                Text(transactions.items.length.toString()),
                              ],
                            ),
                            VerticalDivider(
                              indent: 7,
                              endIndent: 7,
                            ),
                            Column(
                              children: [
                                Icon(
                                  Icons.arrow_upward,
                                  color: Colors.lightGreen,
                                ),
                                Text(
                                    transactions.incomeTransCount().toString()),
                              ],
                            ),
                            VerticalDivider(
                              indent: 7,
                              endIndent: 7,
                            ),
                            Column(
                              children: [
                                Icon(
                                  Icons.arrow_downward,
                                  color: Colors.red,
                                ),
                                Text(transactions
                                    .paymentTransCount()
                                    .toString()),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              OverviewGridCard(
                                  "TOTAL MONTHLY PAYMENTS",
                                  transactions.totalMonthlyPayments(),
                                  Icons.arrow_downward,
                                  Colors.red),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              OverviewGridCard(
                                  "TOTAL MONTHLY INCOME",
                                  transactions.totalMonthlyIncome(),
                                  Icons.arrow_upward,
                                  Colors.lightGreen)
                            ],
                          ),
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              OverviewGridCard(
                                  "TOTAL YEARLY PAYMENTS",
                                  transactions.totalYearlyPayments(),
                                  Icons.arrow_downward,
                                  Colors.red),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              OverviewGridCard(
                                  "TOTAL YEARLY INCOME",
                                  transactions.totalYearlyIncome(),
                                  Icons.arrow_upward,
                                  Colors.lightGreenAccent),
                            ],
                          ),
                        ),
                      ],
                    ),
                    _buildChart(),
                  ],
                ),
              ));
  }
}
