class Transaction {
  late String id;
  late String title;
  late String desc;
  late double amount;
  late List<String> tags;
  late TransactionType type;
  late TransactionFrequency frequency;


  Transaction({required this.id, required this.title, required this.desc, required this.amount, required this.tags, required this.type,
      required this.frequency});

  static getStringAsType(String typeString) {
    switch (typeString) {
      case "TransactionType.Income":
        return TransactionType.Income;
      case "TransactionType.Payment":
        return TransactionType.Payment;
    }
  }

  static getStringAsFrequency(String frequencyString) {
    switch (frequencyString) {
      case "TransactionFrequency.Yearly":
        return TransactionFrequency.Yearly;
      case "TransactionFrequency.Monthly":
        return TransactionFrequency.Monthly;
      case "TransactionFrequency.Bimonthly":
        return TransactionFrequency.Bimonthly;
      case "TransactionFrequency.OnceOnly":
        return TransactionFrequency.OnceOnly;
    }
  }

  static getStringAsTagsList(String tagsString) {
    return tagsString.substring(1, tagsString.length - 1).split(",");
  }
}

enum TransactionType {
  Income,
  Payment,
}

enum TransactionFrequency {
  Yearly,
  Monthly,
  Bimonthly,
  OnceOnly,
}
