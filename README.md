# pay_track

A Flutter App to track your payments

It's a very simple income / payment tracking app.
It also shows graphs of how many percent of your income is going to payments etc.
This is the very first app which I wrote, I would love to hear your feedback.. 
Bug reports, feature requests, suggestions are very welcome.

Soon to be available in F-Droid

[<img src="https://f-droid.org/badge/get-it-on.png"
      alt="Get it on F-Droid"
      height="80">]()
      

![mainscreen](./screenshots/Screenshot-1.png)

![transactions](./screenshots/Screenshot-2.png)

![newTransactionScreen](./screenshots/Screenshot-3.png)
